name = "dev"
region = "eu-west-1"
password = "123"
vpc_cidr =  "11.0.0.0/16"
public_1_ip = "false"
public_2_ip = "false"
subnet_cidr = {
    "be1": "11.0.0.0/24",
    "be2": "11.0.1.0/24",
    "fe1": "11.0.2.0/24",
    "fe2": "11.0.3.0/24"
  }
docker_ports = [
    {
      "external": 8300,
      "internal": 8300,
      "protocol": "tcp"
    }
  ]
